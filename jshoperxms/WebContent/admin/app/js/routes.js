/**
 * Defines the main routes in the application.
 * The routes you see here will be anchors '#/' unless specifically configured otherwise.
 */

define(['./app'], function (app) {
    'use strict';
    return app.config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/index', {
            templateUrl: 'app/index/indexmain.html',
            controller: 'index'
        });
        /**
         * 商品类型管理路由
         */
        $routeProvider.when('/goodstypement',{
        	templateUrl:'app/goods/goodstypement.html',
        	controller:'goodstypelist'
        });
        
        $routeProvider.when('/goodstype',{
        	templateUrl:'app/goods/goodstype.html',
        	controller:'goodstype'
        });
    }]);
});
