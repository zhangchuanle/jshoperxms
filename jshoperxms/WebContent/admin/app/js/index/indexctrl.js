define(['./module'],function(indexmodule){
	'use strict';
	indexmodule.directive("topnav", function() {
		return {
			restrict : 'E',
			templateUrl : 'app/tpls/top_nav_tpl.html',
			replace : true
		}
	});
	indexmodule.directive("leftnav", function() {
		return {
			restrict : 'E',
			templateUrl : 'app/tpls/left_nav_tpl.html'
		}
	});
	indexmodule.directive("footernav", function() {
		return {
			restrict : 'E',
			templateUrl : 'app/tpls/footer_tpl.html'
		}
	});
	indexmodule.controller('index', [ '$scope', '$http', function($scope, $http) {
		$scope.title = "首页";
	} ]);
});

