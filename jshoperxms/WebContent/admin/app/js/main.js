require.config({
	paths:{
		'angular':'../lib/angular/angular',
		'angular-route':'../lib/angular-route/angular-route',
		'angular-resource':'../lib/angular-resource/angular-resource',
		'jquery':'../lib/jquery/jquery',
		'domReady':'../lib/domReady/domReady',
		'datatables':'../lib/angular-datatables/dist/angular-datatables',
		'datatablesbootstrap':'../lib/datatables/media/js/dataTables.bootstrap',
		'jquerydatatable':'../lib/datatables/media/js/jquery.dataTables',
		'bootstrap3':'../lib/bootstrap/dist/js/bootstrap'
	},
	shim:{
		'angular':{
			exports:'angular'
		},
		'angular-route':{
			deps:['angular']
		},
		'angular-resource':{
			deps:['angular']
		},
		'jquery':{
			exports:'jquery'
		},
		'bootstrap3':{
			deps:['jquery'],
			exports:'bootstrap3'
		},
//		"jquerydatatable":{
//			exports:'jquerydatatable'
//		},
//		'datatablesbootstrap':{
//			
//			exports:'datatablesbootstrap'
//		},
		'datatables':{
			deps:['jquery','jquerydatatable','datatablesbootstrap'],
			exports:'datatables'
		}
	},
	waitSeconds:0,
	deps:['./bootstrap']
});
