package com.jshoperxms.action.mall.backstage.vo;

import java.io.Serializable;

/**
 * 商品类型参数vo
* @ClassName: GoodsTypeParamsVo 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author jcchen
* @date 2015年10月4日 下午11:44:14 
*
 */
public class GoodsTypeParamsVo implements Serializable{

	private String id;
	private String paranname;
	private String sort;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getParanname() {
		return paranname;
	}
	public void setParanname(String paranname) {
		this.paranname = paranname;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	
}
