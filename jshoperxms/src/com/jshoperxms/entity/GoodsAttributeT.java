package com.jshoperxms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the goods_attribute_t database table.
 * 
 */
@Entity
@Table(name="goods_attribute_t")
@NamedQuery(name="GoodsAttributeT.findAll", query="SELECT g FROM GoodsAttributeT g")
public class GoodsAttributeT implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String goodsattributeid;

	@Column(name="ATTRIBUTE_INDEX")
	private String attributeIndex;

	@Column(name="ATTRIBUTE_TYPE")
	private String attributeType;

	@Lob
	private String attributelist;

	@Temporal(TemporalType.TIMESTAMP)
	private Date createtime;

	private String creatorid;

	@Column(name="GOODS_TYPE_ID")
	private String goodsTypeId;

	@Column(name="GOODS_TYPE_NAME")
	private String goodsTypeName;

	private String goodsattributename;

	private String issametolink;

	private String issearch;

	private String sort;

	private String state;

	public GoodsAttributeT() {
	}

	public String getGoodsattributeid() {
		return this.goodsattributeid;
	}

	public void setGoodsattributeid(String goodsattributeid) {
		this.goodsattributeid = goodsattributeid;
	}

	public String getAttributeIndex() {
		return this.attributeIndex;
	}

	public void setAttributeIndex(String attributeIndex) {
		this.attributeIndex = attributeIndex;
	}

	public String getAttributeType() {
		return this.attributeType;
	}

	public void setAttributeType(String attributeType) {
		this.attributeType = attributeType;
	}

	public String getAttributelist() {
		return this.attributelist;
	}

	public void setAttributelist(String attributelist) {
		this.attributelist = attributelist;
	}

	public Date getCreatetime() {
		return this.createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public String getCreatorid() {
		return this.creatorid;
	}

	public void setCreatorid(String creatorid) {
		this.creatorid = creatorid;
	}

	public String getGoodsTypeId() {
		return this.goodsTypeId;
	}

	public void setGoodsTypeId(String goodsTypeId) {
		this.goodsTypeId = goodsTypeId;
	}

	public String getGoodsTypeName() {
		return this.goodsTypeName;
	}

	public void setGoodsTypeName(String goodsTypeName) {
		this.goodsTypeName = goodsTypeName;
	}

	public String getGoodsattributename() {
		return this.goodsattributename;
	}

	public void setGoodsattributename(String goodsattributename) {
		this.goodsattributename = goodsattributename;
	}

	public String getIssametolink() {
		return this.issametolink;
	}

	public void setIssametolink(String issametolink) {
		this.issametolink = issametolink;
	}

	public String getIssearch() {
		return this.issearch;
	}

	public void setIssearch(String issearch) {
		this.issearch = issearch;
	}

	public String getSort() {
		return this.sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

}