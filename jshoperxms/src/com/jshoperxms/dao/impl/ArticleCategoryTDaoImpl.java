package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.ArticleCategoryTDao;
import com.jshoperxms.entity.ArticleCategoryT;

@Repository("articleCategoryTDao")
public class ArticleCategoryTDaoImpl extends BaseTDaoImpl<ArticleCategoryT> implements ArticleCategoryTDao {

	private static final Logger log = LoggerFactory.getLogger(ArticleCategoryTDaoImpl.class);

}