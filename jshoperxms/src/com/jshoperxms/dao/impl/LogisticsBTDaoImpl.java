package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.LogisticsBTDao;
import com.jshoperxms.entity.LogisticsBusinessT;


@Repository("logisticsBTDao")
public class LogisticsBTDaoImpl extends BaseTDaoImpl<LogisticsBusinessT> implements LogisticsBTDao {


	private static final Logger log = LoggerFactory.getLogger(GoodsAttributeTDaoImpl.class);


}
