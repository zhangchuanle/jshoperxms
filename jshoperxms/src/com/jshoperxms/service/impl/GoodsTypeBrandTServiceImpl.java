package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.GoodsTypeBrandT;
import com.jshoperxms.service.GoodsTypeBrandTService;

@Service("goodsTypeBrandTService")
@Scope("prototype")
public class GoodsTypeBrandTServiceImpl extends
		BaseTServiceImpl<GoodsTypeBrandT> implements GoodsTypeBrandTService {


}
