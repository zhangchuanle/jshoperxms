package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.OrderInvoiceT;
import com.jshoperxms.service.OrderInvoiceTService;

@Service("orderInvoiceTService")
@Scope("prototype")
public class OrderInvoiceTServiceImpl extends BaseTServiceImpl<OrderInvoiceT>implements OrderInvoiceTService {

}
