package com.jshoperxms.service.impl;


import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jshoperxms.dao.BrandTDao;
import com.jshoperxms.entity.BrandT;
import com.jshoperxms.entity.GoodsTypeBrandT;
import com.jshoperxms.service.BrandTService;
import com.jshoperxms.service.GoodsTypeBrandTService;

@Service("brandTService")
@Scope("prototype")
public class BrandTServiceImpl extends BaseTServiceImpl<BrandT>implements BrandTService {
	@Resource
	private BrandTDao brandTDao;
	@Resource
	private GoodsTypeBrandTService goodsTypeBrandTService;

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void saveBrandTransaction(BrandT brand,
			GoodsTypeBrandT goodsTypeBrandT) {
		this.brandTDao.save(brand);
		this.goodsTypeBrandTService.save(goodsTypeBrandT);
		
	}
}
