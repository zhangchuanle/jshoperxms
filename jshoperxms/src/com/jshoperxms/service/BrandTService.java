package com.jshoperxms.service;


import com.jshoperxms.entity.BrandT;
import com.jshoperxms.entity.GoodsTypeBrandT;

public interface BrandTService extends BaseTService<BrandT>{

	/**
	 * 保存品牌及商品品牌和商品类型的关系
	 * @param brandT
	 * @param goodsTypeBrandT
	 */
	public void saveBrandTransaction(BrandT brandT,GoodsTypeBrandT goodsTypeBrandT);
	
}
